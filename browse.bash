#!/usr/bin/env bash
# shellcheck shell=bash
#
# pass browse - Password Store Extension (https://www.passwordstore.org/)
# Copyright (C) 2020 Bart Bakker <bart@thesoftwarecraft.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

readonly VERSION="0.9"

set -eo pipefail

browse_show_version() {
    cat <<_EOF
$PROGRAM $COMMAND $VERSION - A pass extension to browse through your passwords.
_EOF
}

browse_show_usage() {
    browse_show_version
    echo
    cat <<_EOF
Usage:
    $PROGRAM $COMMAND [-1,--select-1] [-c,--clip] [--info] [pass-name]

Options:
    -1, --select-1  Don't start interactive finder if <pass-name> has only one match.
    -c, --clip      Do no print the password but instead copy the line to the
                    clipboard
    -i, --info      Show additional information of your passwords while browsing and
                    of the selected entry when --clip is used. Requires decrypting
                    entries while browsing.
    -V, --version   Print version information
    -h, --help      Print this message and exit
_EOF
}

CLIP=0
INFO=0
SELECT_1=0

browse_run() {
    query="$*"
    pass_args=()
    fzf_args=(
        '--bind=ctrl-j:accept,ctrl-k:kill-line'
        --inline-info
        --no-multi
        --tiebreak=begin
    )

    [ $CLIP == 0 ] || pass_args+=("-c")
    [ $INFO == 0 ] || fzf_args+=(--preview='pass show {} | tail -n+2')
    [ $SELECT_1 == 0 ] || fzf_args+=(--select-1)

    if path=$(
            find "$PREFIX" -type f -name '*.gpg' \
                | sed "s:^${PREFIX}/\(.*\)\.gpg\$:\1:" \
                | fzf "${fzf_args[@]}" --query "$query"
           ); then
        if [ $CLIP != 0 ] && [ $INFO != 0 ]; then
            pass show "$path" | tail -n+2
        fi
        pass show "${pass_args[@]}" "$path"
    else
        die
    fi
}

browse_main() {
    opts="$($GETOPT -o 'c1hiV' -l 'clip,info,select-1,help,version' -n "$PROGRAM $COMMAND" -- "$@")"
    test $? == 0 || die
    eval set -- "$opts"

    while true; do case $1 in
        -c|--clip) CLIP=1; shift ;;
        -i|--info) INFO=1; shift ;;
        -1|--select-1) SELECT_1=1; shift ;;
        -h|--help) shift; browse_show_usage; exit 0 ;;
        -V|--version) shift; browse_show_version; exit 0 ;;
        --) shift; break ;;
    esac; done

    browse_run "$*"
}

browse_main "$@"
