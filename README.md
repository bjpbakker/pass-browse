# pass browse

**A [pass] extension that provides an easy terminal browser**

## Description

Searching through your `pass` entries and copying the password in the terminal
can be cumbersome at times. `pass browse` provides a convenient way to fuzzy
search entries with [fzf], view the notes and copy the password.

## Usage

```
pass browse 0.9 - A pass extension to browse through your passwords.

Usage:
    pass browse [-1,--select-1] [-c,--clip] [--info] [pass-name]

Options:
    -1, --select-1  Don't start interactive finder if <pass-name> has only one match.
    -c, --clip      Do no print the password but instead copy the line to the
                    clipboard
    -i, --info      Show additional information of your passwords while browsing and
                    of the selected entry when --clip is used. Requires decrypting
                    entries while browsing.
    -V, --version   Print version information
    -h, --help      Print this message and exit
```

## Installation

**Requirements**

* `pass 1.7` or up
* `fzf`

**From git**

```
git clone https://gitlab.com/bjpbakker/pass-browse
cd pass-browse
sudo make install
```

**Latest release**

```
curl -o pass-browse-v0.9.tar.gz https://gitlab.com/bjpbakker/pass-browse/-/archive/v0.9/pass-browse-v0.9.tar.gz
tar xzf pass-browse-v0.9.tar.gz
cd pass-browse-v0.9.tar.gz
sudo make install
```

## License

Copyright 2020-2023, Bart Bakker.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.

[fzf]: https://github.com/junegunn/fzf
[pass]: https://www.passwordstore.org/
