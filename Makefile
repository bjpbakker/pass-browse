PREFIX ?= /usr
LIBDIR ?= $(PREFIX)/lib
SYSTEM_EXTENSION_DIR ?= $(LIBDIR)/password-store/extensions

default:

install:
	install -D -m755 browse.bash $(SYSTEM_EXTENSION_DIR)/browse.bash
