# Releases

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.9] - 2023-07-07

### Changed

- Print additional information of the selected password entry on stdout when
  copying to clipboard (`--clip`) and information (`--info`) is enabled.

### Removed

- Deleted the deprecated `--enable-info` option. Use `--info` instead.

## [v0.8] - 2023-04-23

### Changed

- Rename option `--enable-info` to `--info` (shorthand `-i`). Print a
  deprecation warning when `--enable-info` is used.

### Fixed

- No longer print the full list of password store entries When no entry is
  selected (e.g. when the query does not match or when canceled).

## [v0.7] - 2022-05-27

### Added

- Allow skipping interactive finder when initial pass-name yields only
  one match.

### Changed

- Improve sub-command argument handling and exit on any failure.

## [v0.6] - 2020-11-19

### Added

- Release notes

### Fixed

- Fix shellcheck lint error

## [v0.5] - 2020-11-19

### Changed

- Bind keys C-j (accept) and C-k (kill-line) in `fzf`

### Fixed

- Using `pass-browse` without `--clip` resulted in the password tree being
  printed. This is fixed so it shows the accepted entry on stdout again.

## [v0.4] - 2020-10-01

### Added

- Use extra program arguments as initial search query. E.g. `pass browse
  my-pass` starts with a search on `my-pass`.

### Fixed

- Improve program arguments in usage output.

## [v0.3] - 2020-09-28

### Fixed

- Fix running as `pass` extension.

## [v0.2] - 2020-09-28

### Added

- Document installation in README.

### Fixed

- Fix password listing when run as `pass` extension.

## [v0.1] - 2020-09-28

### Added

- Initial release.

[v0.1]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.1
[v0.2]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.2
[v0.3]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.3
[v0.4]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.4
[v0.5]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.5
[v0.6]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.6
[v0.7]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.7
[v0.8]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.8
[v0.9]: https://gitlab.com/bjpbakker/pass-browse/-/tags/v0.9
